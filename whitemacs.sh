#!/bin/sh

cfg_path="$HOME"/mac_iptables_config
addrs=$(cat $cfg_path)

# Delete addrs that have been deleted from on run to another
# TODO

i=$(expr "$addrs" | wc -l)

# Loop and whitelist
for addr in $addrs; do
    # sudo iptables --append INPUT -m mac --mac-source "$addr" -j DROP
    # DEBUG :
    # echo "DEBUG: $addr"
    echo -n "$i.."
    i=$(expr $i - 1)
done

echo -n "."

echo $'\n\nDone.'
echo $'\n'
echo 'INFO: You can list all the whitelisted addrs with the following command `sudo iptables --list`'
